rv
==

RV is a bike routing engine on top of OSM data and SRTM elevation data. Herve
is the web frontend part.

RV uses a physical model of the bike under the hood, to try to figure out best
routes which minimizes energy provided by the biker.

rv is now deprecated and a rework has been started. See
[rv2](https://gitlab.crans.org/leger/rv2), which should be faster, easier and
more reliable.

## License

BSD 2-clause "Simplified" License.
