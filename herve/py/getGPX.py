#!/usr/bin/python

import cgi
import cgitb
import sys

cgitb.enable(display=1, logdir="/tmp/hervelog/")
#cgitb.enable()

import rv

form = cgi.FieldStorage()

try:
    jid=int(form['jid'].value)
    minimal=bool(int(form['minimal'].value))
    step=int(form['step'].value)
    nptsmax=int(form['nptsmax'].value)
except (KeyError, ValueError):
    sys.exit(1)

gpxxml = rv.getGPX(jid,minimal,step,nptsmax)

print "Content-Disposition: attachment; filename=rv_result_%d.gpx" % jid
print "Content-type: application/gpx+xml\n"

print gpxxml

sys.stdout.flush()


