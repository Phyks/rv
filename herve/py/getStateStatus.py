#!/usr/bin/python

import cgi
import cgitb
import sys

import psycopg2

import json

#cgitb.enable(display=0, logdir="/tmp/hervelog/")
cgitb.enable()

from config import *

form = cgi.FieldStorage()

try:
    jid=int(form['jid'].value)
except (KeyError, ValueError):
    sys.exit(1)


db = psycopg2.connect(dbstr)
cur = db.cursor()

cur.execute("SELECT state,status FROM rv_jobs WHERE jid=%s;",(jid,));

ligne=cur.fetchone()

state=ligne[0]
status=ligne[1]

print "Content-type: application/json\n"
print json.dumps({'State':state,'Status':status})

sys.stdout.flush()



