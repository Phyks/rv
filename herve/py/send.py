#!/usr/bin/python

import cgi
import cgitb
import time
import sys

import psycopg2
import subprocess
import daemon

import json

limitjobs = 4

cgitb.enable()

from config import *


form = cgi.FieldStorage()

rho = 1.204

try:
    criterion=form['criterion'].value
except (KeyError, ValueError):
    criterion='e'

try:
    Cr=float(form['Cr'].value)
except (KeyError, ValueError):
    Cr=.008

try:
    SCx=float(form['SCx'].value)
except (KeyError, ValueError):
    SCx=.45

try:
    mass=float(form['mass'].value)
except (KeyError, ValueError):
    mass=.008

try:
    speedref=float(form['speedref'].value)
except (KeyError, ValueError):
    speedref=25.

try:
    winddir=float(form['winddir'].value)
    windspeed=float(form['windspeed'].value)
except (KeyError, ValueError):
    winddir=0.
    windspeed=0.

# verifs

if Cr<=0 or SCx<=0 or mass<=0 or speedref<=0:
    sys.exit(1)

if criterion!='e' and criterion!='d' and criterion !='p':
    sys.exit(1)

# points

lons=form.getlist('lon')
lats=form.getlist('lat')

if len(lons)!=len(lats) or len(lons)<2:
    sys.exit(1)

# all right

db = psycopg2.connect(dbstr)
cur = db.cursor()

cur.execute("SELECT jid FROM rv_jobs WHERE state=1 OR state=2;");

if cur.rowcount >= limitjobs:
    print "Content-type: application/json\n"
    print json.dumps({'jid':-1})
    sys.exit(0)



req="INSERT INTO rv_jobs (expire,cr,scx,criterion,mass,rho,speedref,winddir,windspeed,state) VALUES ('tomorrow',%s,%s,%s,%s,%s,%s,%s,%s,0) RETURNING jid;"

cur.execute(req,(Cr,SCx,criterion,mass,rho,speedref,winddir,windspeed))

jid=cur.fetchone()[0]

for i in range(len(lons)):

    try:
        lon = float(lons[i])
        lat = float(lats[i])
    except ValueError:
        sys.exit(1)

    req="INSERT INTO rv_waypoints (jid,rank,geom) VALUES (%d,%d,'SRID=4326;POINT(%.8f %.8f)'::geometry);" % (jid,i,lon,lat)
    cur.execute(req)

# GO
cur.close()
db.commit()
db.close()

print "Content-type: application/json\n"
print json.dumps({'jid':jid})

sys.stdout.flush()

subprocess.Popen(["/sbin/start-stop-daemon",'-S','-b','-p','/tmp/does-not-exists','--exec',rv_route,'--', dbstr, ("%d"%jid)])



