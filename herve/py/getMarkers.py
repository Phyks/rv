#!/usr/bin/python

import cgi
import cgitb
import sys

import psycopg2

import json




#cgitb.enable(display=0, logdir="/tmp/hervelog/")
cgitb.enable()

import rv

form = cgi.FieldStorage()

try:
    jid=int(form['jid'].value)
except (KeyError, ValueError):
    sys.exit(1)

markers = rv.getMarkers(jid)

print "Content-type: application/json\n"
print json.dumps(markers);

sys.stdout.flush()



