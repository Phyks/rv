#!/usr/bin/python

import cgi
import cgitb
import sys

import psycopg2

import json
import subprocess

#cgitb.enable(display=0, logdir="/tmp/hervelog/")
cgitb.enable()

from config import *

form = cgi.FieldStorage()

try:
    jid=int(form['jid'].value)
except (KeyError, ValueError):
    sys.exit(1)


db = psycopg2.connect(dbstr)
cur = db.cursor()

cur.execute("SELECT state,pid FROM rv_jobs WHERE jid=%s;",(jid,));

ligne=cur.fetchone()

state=ligne[0]
pid=ligne[1]

if(state!=1 and state!=2):
    print "Content-type: application/json\n"
    print json.dumps({})

if(pid>0):
    subprocess.Popen(['/bin/kill',"%u"%pid]);
    cur.execute("UPDATE rv_jobs SET state=99 WHERE jid=%s;",(jid,));

cur.close()
db.commit()
db.close()

print "Content-type: application/json\n"
print json.dumps({'jid':0})

sys.stdout.flush()



