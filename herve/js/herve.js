var herve = {
    map: {},
    points: [],
    jid: 0,
    State: 0,
    Status: 0,
    displaytimer: {},
    global_results: {},
    actualLayer: {},
    actualLayerNumber: -1,
    layers: [{},{},{},{},{},{},{},{}],
    search_markers: []
};

function load()
{
    load_map();

    var anch = location.hash;

    if(anch.length>1)
    {
        jid=anch.slice(1);
        herve.jid=jid;
        drawMarkers();
        drawResults();
    }
}

function load_map() {
  herve.map = new L.Map('map', {zoomControl: true});
  L.control.scale({metric: true, imperial: false}).addTo(herve.map);

  var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
  var osmAttribution = 'Map data &copy; 2013 <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
  var osm = new L.TileLayer(osmUrl, {maxZoom: 18, attribution: osmAttribution});
  
  var osmfrUrl = 'http://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png';
  var osmfr = new L.TileLayer(osmfrUrl, {maxZoom: 18, attribution: osmAttribution});

  herve.map.setView(new L.LatLng(47, 3), 6);
  
  var baseLayers = {
        "OpenStreetMap": osm,
        "OpenStreetMap France": osmfr,
  };

  new L.control.layers(baseLayers).addTo(herve.map);
  
  herve.map.addLayer(osmfr);

/*  herve.map.on('contextmenu', function(e) {
      new L.marker(e.latlng)
            .bindLabel("Point " + herve.b, { noHide: true })
            .addTo(herve.map)
            .showLabel();
      herve.b+=1
  });
*/
  herve.map.on('contextmenu',rightClick);
  herve.map.on('popupclose',function() {hideAllLabels(); addLabelHandlers(); unbindAllPopups();});
  herve.map.on('viewreset',redraw);
}

function delete_search_markers()
{
    $('#recherche_results').html('');
    for( var i=0;i<herve.search_markers.length;i++)
    {
        herve.map.removeLayer(herve.search_markers[i]);
    }
    herve.search_markers=[]

}


function rechercher()
{
    delete_search_markers();
    var txt=$('input[name=recherche_txt]').val();

    var url="http://nominatim.openstreetmap.org/search?email=jben+herve@jben.info&format=json&countrycodes=fr&limit=5&q=" + txt;
    $('#recherche_results').html('<p>Recherche en cours…</p>');

    $.getJSON(url,function(data) {
        if(data.length<1)
        {
            $('#recherche_results').html('<p>Aucun résultat trouvé</p>');
            return false;
        }
    
        $('#recherche_results').html('<p>'+data.length+' resultats.'
            + '<br /><a href="#" onclick="delete_search_markers(); return false;">Effacer les resultats.</a>'
            + '</p>'
            );


        var red_icon = L.icon({
                iconUrl: 'images/marker-icon-red.png',
                iconRetinaUrl: 'images/marker-icon-2x-red.png',
                shadowUrl: 'images/marker-shadow.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41]
        });

        for(var i=0;i<data.length;i++)
        {
            var m = new L.marker([data[i].lat,data[i].lon],{icon:red_icon});
            m.on('contextmenu',function(e){
                e.target.bindPopup(preparepopup(e.target.getLatLng().lat,e.target.getLatLng().lng)).openPopup();
            });

            m.addTo(herve.map);
            herve.search_markers.splice(0,0,m);
        }
    });
}

function showAllLabels()
{
    for(var i=0;i<herve.points.length;i++)
    {
        herve.points[i].showLabel();
    }
}
function hideAllLabels()
{
    for(var i=0;i<herve.points.length;i++)
    {
        herve.points[i].hideLabel();
    }
}

function unbindAllPopups()
{
    return false;
    for(var i=0;i<herve.points.length;i++)
    {
        herve.points[i].unbindPopup();
    }

    for(var i=0;i<herve.search_markers.length;i++)
    {
        herve.search_markers.unbindPopup();
    }
}

function preparepopup(lat,lon)
{
    var text = '<p>Ajouter ce point à l\'itinéraire ?</p>'
        + '<ul>';

    if(herve.points.length==0)
    {
        text += '<li><a href="#" onclick="ajouterPoint(0,'
            +lat+','+lon
            +');return false;">Ajouter</a></li>';
    }
    else
    {
        for(var i=0;i<herve.points.length;i++)
        {
            if(i==0)
            {
                text += '<li><a href="#" onclick="ajouterPoint(0,'
                    +lat+','+lon
                    +');return false;">Avant le point 1</a></li>';
            }
            else
            {
                text += '<li><a href="#" onclick="ajouterPoint('+i
                    +','+lat+','+lon
                    +');return false;">Entre les points '+(i)+' et '+(i+1)+'</a></li>';
            }
        }
        text += '<li><a href="#" onclick="ajouterPoint('+herve.points.length
            +','+lat+','+lon
            +');return false;">Après le point '+(herve.points.length)+'</a></li>';
    }

    text += '</ul>'
    
    showAllLabels();
    removeLabelHandlers();

    return text;
}

function rightClick(e) {
    new L.popup()
        .setLatLng(e.latlng)
        .setContent(preparepopup(e.latlng.lat,e.latlng.lng))
        .openOn(herve.map);
}

function updateMarkers(data)
{
    for(var i=0;i<data.length;i++)
    {
        var mark = new L.marker(data[i]).addTo(herve.map);
        mark.on('contextmenu', rightClickPoint);
        herve.points.splice(i, 0, mark);
    }
    addLabels();
    addLabelHandlers();
}


function ajouterPoint(pos,lat,lon) 
{
    herve.map.closePopup();

    var mark = new L.marker([lat,lon]).addTo(herve.map);
    mark.on('contextmenu', rightClickPoint);
    herve.points.splice(pos, 0, mark);

    addLabels();
    addLabelHandlers();
    mettreAJourStatus();
}

function removePoint(pos)
{
    var marker=herve.points[pos];
    marker.closePopup();
    herve.points.splice(pos, 1);
    herve.map.removeLayer(marker);
    addLabels();
    mettreAJourStatus();
}

function rightClickPoint(e)
{
    var pos = herve.points.indexOf(e.target);

    e.target.bindPopup("<a href='#' onclick='removePoint("+pos+");return false;'>Supprimer ?</a>").openPopup();

}


function addLabels()
{
    for(var i=0;i<herve.points.length;i++)
    {
        herve.points[i].unbindLabel();
        herve.points[i].bindLabel("Point "+(i+1));
    }
}

function addLabelHandlers()
{
    for(var i=0;i<herve.points.length;i++)
    {
        herve.points[i]._removeLabelRevealHandlers();
        herve.points[i].on('mouseover', showAllLabels);
        herve.points[i].on('mouseout', hideAllLabels);
    }
}

function removeLabelHandlers()
{
    for(var i=0;i<herve.points.length;i++)
    {
        herve.points[i]._removeLabelRevealHandlers();
        herve.points[i].off('mouseover', showAllLabels);
        herve.points[i].off('mouseout', hideAllLabels);
    }
}


function hideAdvancedParameters()
{
    $('#advanced_parameters').hide();
    $('#advanced_parameters_txt').html('<p><a href="#" onclick="showAdvancedParameters();return false;">Paramètres avancés</a></p>');
}

function showAdvancedParameters()
{
    $('#advanced_parameters').show();
    $('#advanced_parameters_txt').html('<p><a href="#" onclick="hideAdvancedParameters();return false;">Masquer les paramètres avancés</a></p>');
}


function mettreAJourStatus(msg)
{
    var msg = msg || '';

    if(herve.jid>0)
    {
        window.clearInterval(herve.displaytimer);
        $.getJSON('py/kill.py?jid='+herve.jid,function(data){
            herve.jid=data.jid;
        });
    }
    var ok=true;
    var txt='';

    var mass=document.getElementById('mass').value;
    var speedref=document.getElementById('speedref').value;
    var SCx=document.getElementById('SCx').value;
    var Cr=document.getElementById('Cr').value;
    var winddir=document.getElementById('winddir').value;
    var windspeed=document.getElementById('windspeed').value;

    if(!($.isNumeric(mass) && mass>0))
    {
        txt+='<p>La masse doit être positive</p>';
        ok=false;
    }
    if(!($.isNumeric(speedref) && speedref>0))
    {
        txt+='<p>La vitesse doit être positive</p>';
        ok=false;
    }
    if(!($.isNumeric(SCx) && SCx>0))
    {
        txt+='<p>SCx doit être positif</p>';
        ok=false;
    }
    if(!($.isNumeric(Cr) && Cr>0))
    {
        txt+='<p>Cr doit être positif</p>';
        ok=false;
    }
    if(!($.isNumeric(winddir) && $.isNumeric(windspeed)))
    {
        txt+='<p>Ce vent est étrange</p>';
        ok=false;
    }


    if(herve.points.length<2)
    {
        txt+='<p>Ajoutez des points !<br />(Le clic droit est votre ami)</p>';
        ok=false;
    }


    if(ok)
    {
        $('#status').html(msg+'<p><input type="button" onclick="compute(); return false;" value="Calculer l\'itinéraire" /></p>');
    }
    else
    {
        $('#status').html(txt);
    }

    return ok;
}

function updateParameters(data)
{
    $('input[name=mass]').val(data.mass);
    $('input[name=speedref]').val(data.speedref);
    $('input[name=SCx]').val(data.SCx);
    $('input[name=Cr]').val(data.Cr);
    $('input[name=winddir]').val(data.winddir);
    $('input[name=windspeed]').val(data.windspeed);
    $('input:radio[name=mode][value='+data.criterion+']').click();
    $('#status').html('<p>Chargement en cours…'+data.mass+'</p>');
}

function compute()
{
    delete_search_markers();

    var mass=document.getElementById('mass').value;
    var speedref=document.getElementById('speedref').value;
    var SCx=document.getElementById('SCx').value;
    var Cr=document.getElementById('Cr').value;
    var winddir=document.getElementById('winddir').value;
    var windspeed=document.getElementById('windspeed').value;
    var mode=$('input[name=mode]:checked').val();
    
    if(!mettreAJourStatus())
    {
        return(false);
    }

    var url='py/send.py'
        + '?mass='+mass
        + '&speedref='+speedref
        + '&SCx='+SCx
        + '&Cr='+Cr
        + '&winddir='+winddir
        + '&windspeed='+windspeed
        + '&criterion='+mode;

    for(var i=0;i<herve.points.length;i++)
    {
        var ll=herve.points[i].getLatLng();
        url+='&lat='+ll.lat;
        url+='&lon='+ll.lng;
    }

    herve.jid=0;
    herve.State=0;
    $.getJSON(url, function(data) {
        herve.jid=data.jid;
    });

    $('#status').html('<p>Routage en cours…</p>');
    $('#global_results').html('');
    $('#links').html('');
    herve.displaytimer = window.setInterval("updateRoutingStatus()",500);
}

function updateRoutingStatus()
{
    var txt='';
    if(herve.jid==-1)
    {
        window.clearInterval(herve.displaytimer);
        mettreAJourStatus('<p>Serveur surchargé</p>');
    }

    if(herve.jid>0)
    {
        var url='py/getStateStatus.py?jid='+herve.jid;
        $.getJSON(url,function(data) {
            herve.State=data.State;
            herve.Status=data.Status;
        });
        
        if(herve.State==0)
        {
            txt = '<p>Lancement…</p>';
        }

        if(herve.State==1)
        {
            txt = '<p>Recherche des points…</p>';
        }
        if(herve.State==2)
        {
            txt = '<p>Routage… ' + (herve.Status*100).toFixed(1) +'%</p>';
        }
        if(herve.State==3)
        {
            txt = '<p>Routage terminé.</p>';
            window.clearInterval(herve.displaytimer);
            drawResults();
        }
        if(herve.State==17)
        {
            txt = '<p>Erreur: Points non connexes</p>';
        }
        if(herve.State==18)
        {
            txt = '<p>Erreur: Distance trop longue</p>';
        }
        if(herve.State==19)
        {
            txt = '<p>Erreur: WTF (19)</p>';
        }
        if(herve.State==21)
        {
            txt = '<p>Erreur: Inconsistant db (21)</p>';
        }
        if(herve.State==22)
        {
            txt = '<p>Erreur: Inconsistant db (22)</p>';
        }

        
        $('#status').html(txt);
    }
}

function drawMarkers()
{
    $.getJSON('py/getParameters.py?jid='+herve.jid, updateParameters);
    $.getJSON('py/getMarkers?jid='+herve.jid, updateMarkers);
}


function drawResults()
{
    $.getJSON('py/getGlobalResults.py?jid='+herve.jid, updateGlobalResults);
    $.getJSON('py/getResults.py?jid='+herve.jid, updateResults);
}

function info(pn)
{
    return '<a href="#" onclick="help(\''+pn+'\'); return false;">infos</a>';
}

function updateResults(data)
{
    var results = data.allres;
    for(var n=0;n<8;n++)
    {
        delete herve.layers[n]
        herve.layers[n] = new L.geoJson(results[n], {
            style: function (feature) {
                return {color: feature.properties.color, opacity: 0.9};
            },
            onEachFeature: function (feature, layer) {
                if(n>=2)
                {
                    var txt='<ul>'
                        +'<li>Distance&nbsp;: '+feature.properties.dist+'&nbsp;km</li>'
                        +'<li>Temps&nbsp;: '+feature.properties.time+'</li>'
                        +'<li>Pente&nbsp;: '+feature.properties.pente+'&nbsp;%</li>'
                        +'<li>Altitude&nbsp;: '+feature.properties.height+'&nbsp;m</li>'
                        +'<li>Vitesse&nbsp;: '+feature.properties.speed+'&nbsp;km/h</li>'
                        +'<li>Dénivelé positif cumulé&nbsp;: '+feature.properties.cum_elev+'&nbsp;m</li>'
                        +'</ul>'
                    layer.bindPopup(txt,{autoPan:false});
                }
            }
        });
    }
    herve.actualLayerNumber=-1
    redraw(42);
    herve.map.fitBounds(data.bounds);
    txt='<p>Routage terminé.</p>'
         +'<p>Cliquez sur le trajet pour obtenir des infos détaillés</p>';
    $('#status').html(txt);
    showLinks()
}

function showLinks()
{
    txt='<p><a href="#' + herve.jid + '">Lien vers l\'itinéraire</a> '
        +'<a href="#" onclick="showGpx(); return false;">Route au format GPX</a>'
        +'</p>'
        +'<hr />';
    $('#links').html(txt);
}

function showGpx()
{
    txt='<ul>'
        +'<li><a href="py/getGPX.py?jid=' + herve.jid + '&minimal=0&step=0&nptsmax=0">GPX avec tous les pts</a></li>'
        +'<li><a href="py/getGPX.py?jid=' + herve.jid + '&minimal=1&step=0&nptsmax=0">GPX avec uniquement les pts d\'itersection</a></li>'
        +'<li><a href="py/getGPX.py?jid=' + herve.jid + '&minimal=0&step=200&nptsmax=50">GPX pour Garmin (50 pt max)</a></li>'
        +'</ul>'
        +'<p><a href="#" onclick="showLinks(); return false;">Retour</a></p>'
        +'<hr />';
    $('#links').html(txt);
}

function updateGlobalResults(results)
{
    herve.global_results = results;

    var txt='<p><ul>'
        +'<li>Distance&nbsp;: '+herve.global_results.dist+'&nbsp;km '+info('dist')+'</li>'
        +'<li>Temps&nbsp;: '+herve.global_results.time+' '+info('time')+'</li>'
        +'<li>Vitesse moyenne&nbsp;: '+herve.global_results.mean_speed+'&nbsp;km/h '+info('mean_speed')+'</li>'
        +'<li>Dénivelé positif cumulé&nbsp;: '+herve.global_results.cum_elev+'&nbsp;m '+info('cum_elev')+'</li>'
        +'<li>Énergie fournie&nbsp;: '+herve.global_results.energy+'&nbsp;kJ '+info('energy')+'</li>'
        +'</ul></p><hr />'
    $('#global_results').html(txt);
}

function redraw(e)
{

    var z=herve.map.getZoom();

    var n;
    if(z<7)
    {
        n=0;
    }
    else if(z<14)
    {
        n=z-7;
    }
    else
    {
        n=7;
    }

    if(n!=herve.actualLayerNumber)
    {

        try
        {
            herve.map.removeLayer(herve.actualLayer);
        }
        catch(err)
        {
        }
        herve.actualLayer = herve.layers[n];
        herve.actualLayerNumber = n;
        try
        {
            herve.map.addLayer(herve.actualLayer);
        }
        catch(err)
        {
        }
    }


}

function help(pn)
{
    var mode='';
    var txt='';

    if(pn=='mass')
    {
        nom='Masse roulante';
        txt="La masse en kg est utilisée pour l'application du modèle physique. La masse à prendre en compte est la masse totale, c'est à dire <strong>cycliste</strong> + <strong>bicyclette</strong> + <strong>bagages</strong>";
    }

    if(pn=='speedref')
    {
        nom='Vitesse sur le plat';
        txt="La vitesse en km/h sur le plat sert à calculer la puissance constante fournie par le cycliste. Il faut rentrer la vitesse tenue par le cycliste sur le plat sans vent sur le long terme.";
    }

    if(pn=='modee')
    {
        nom='Mode d\'optimisation, énergie';
        txt="L'optimisation de l'energie minimise l'énergie totale fournie par le cycliste, cette énergie est calculée en utilisant les autres paramêtres fournis par le modèle. Il s'agit généralemnt d'un bon compromis entre la distance et le dénivelé.";
    }

    if(pn=='moded')
    {
        nom='Mode d\'optimisation, distance';
        txt="L'optimisation de la distance minimise la distance parcourue. Le fait de parcourir la distance la plus faible peut entraîner des itinéraire absurdes vis-à-vis des dénivelés.";
    }

    if(pn=='modep')
    {
        nom='Mode d\'optimisation, dénivelé ascendant total';
        txt="L'optimisation du dénivelé totale ascendant minimise la somme des dénivelés en côte. Cette minimisation eput s'effectuer au prix de détours qui peuvent parfois être très importants.";
    }

    if(pn=='SCx')
    {
        nom='SCx';
        txt="Ce paramêtre en m² est le produit de la surface frontale S, et du coeficient de pénétration dans l'air Cx. La figure 2 de <a href='http://hal.archives-ouvertes.fr/docs/00/24/52/03/PDF/ajp-rphysap_1984_19_4_349_0.pdf'>cet article</a> peut vous donner une idée.";
    }

    if(pn=='Cr')
    {
        nom='Cr';
        txt="Ce parametre sans dimention, correspond au coefficient de roulement du pneu sur la route. Il dépend du type de pneu et du type de route.";
    }

    if(pn=='wind')
    {
        nom='Vent';
        txt='Ces parametres servent à prendre en compte la vitesse et la direction du vent dans le calcul de l\'énergie. Son influence est faible, mais existante, dans le résultat de la méthode de routage dans le cas du mode énergie. Elle n\'est pas faible toutefois sur la valeur de l\'énergie. La direction est la provenance du vent en degrée par rapport au Nord, et la vitesse est la vitesse du vent en km/h.';
    }

    if(pn=='dist')
    {
        nom='Distance';
        txt="Cette valeur est la distance totale parcourue en km.";
    }

    if(pn=='time')
    {
        nom='Temps';
        txt="Cette valeur est le temps total necessaire pour parcourir l'itinéraire. Attention ce temps ne tient pas compte des arrêts éventuels.";
    }

    if(pn=='mean_speed')
    {
        nom='Vitesse moyenne';
        txt="Cette valeur est la vitesse moyenne en km/h sur l'itinéraire. Attention cette vitesse ne tient pas compte des arrêts éventuels.";
    }

    if(pn=='cum_elev')
    {
        nom='Dénivelé positif cumulé';
        txt="Cette valeur est la somme des dénivelés en côte parcourus.";
    }

    if(pn=='energy')
    {
        nom='Énergie fournie';
        txt='Cette valeur est l\'énergie fournie par le cycliste pour parcourir l\'itininéraire. En considérant un rendement du corps humain entre 20% et 25%, pour fournir un kJ, le corps humain dépense entre 0.9 et 1.2 kcal.';
    }

    var html='<p style="text-decoration:underline;">'
        + nom + "</p>"
        + "<p>" + txt + "</p>"
        + '<a href="#" onclick="hide_help(); return false;">Masquer</a>'
        + '<hr />';

    $('#help').html(html);
}

function hide_help()
{
    $('#help').html('');
}


//window.onload = load();
hideAdvancedParameters();
mettreAJourStatus();
load();
