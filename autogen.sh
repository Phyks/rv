#!/bin/sh

aclocal
autoconf
automake --copy --add-missing

rm -r autom4te.cache/
rm aclocal.m4
