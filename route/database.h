#ifndef H_ROUTAGE_DATABASE
#define H_ROUTAGE_DATABASE 1

#include <pqxx/pqxx>
#include <string>
#include <iomanip>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <list>
#include <math.h>
#include "node.h"
#include "model.h"
#include "instant.h"
#include <unistd.h>

#define PREC(N) std::setiosflags(std::ios::fixed) << std::setprecision(N) <<

class database
{
    public:

    pqxx::connection db;
    pqxx::read_transaction txn;
    unsigned int jid;
    
    pqxx::connection dbw;

    database(const std::string & s_filename,unsigned int jid_i) : db(s_filename),txn(db),dbw(s_filename),jid(jid_i) {
        db.prepare("get_node","SELECT ST_X(geom),ST_Y(geom),height FROM rv_nodes WHERE id=$1;");
        db.prepare("get_adj","SELECT to_id FROM rv_edges WHERE from_id=$1;");
    }


    node get_node(const unsigned long long & ull_id);
    std::pair<unsigned long long, unsigned int> get_edge(const unsigned long long & ull_from, const unsigned long long & ull_to);
    std::list<unsigned long long> get_adjoining(const unsigned long long & ull_id);


    double get_height_s(const int & lon_s, const int & lat_s);

    double get_height(const double & lon, const double & lat);

    bool find_one_node(std::pair<double,double> P, bool respect_cc, unsigned long long ull_cc, unsigned long long & ull_result_id, unsigned long long & ull_result_cc, double & dist);

    bool find_nodes(std::list<unsigned long long> &);

    model get_model();

    unsigned int get_state();
    void write_state(unsigned int state);
    void write_status(double status);

    void write_results(std::list<instant> lI, double puissance);
    void write_pid();


};

#endif
