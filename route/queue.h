#ifndef H_QUEUE
#define H_QUEUE 1

#include<queue>

class node_in_queue
{
    public:
    unsigned long long ull_id;
    double d_cout;

    node_in_queue(unsigned long long ull_id_i, double d_cout_i) : ull_id(ull_id_i), d_cout(d_cout_i) {}

    // Attention un noeuds est MIEUX si il a un cout INFERIEUR
    bool operator<(const node_in_queue & q_2) const { return d_cout> q_2.d_cout; }

};

class queue
{
    std::priority_queue<node_in_queue> Q_queue;

    public:
    queue() {}

    bool empty() {return Q_queue.empty();}

    void push(const node_in_queue & q_node) { Q_queue.push(q_node); }
    unsigned long long pop() { unsigned long long ull_id=Q_queue.top().ull_id; Q_queue.pop(); return ull_id;}
};

#endif
