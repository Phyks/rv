algebraic:true$
d_Ec:1/2*d_masse*(d_V1^2-d_V0^2)$
d_V1r:d_V1+d_vent$
d_V0r:d_V0+d_vent$
d_t:2*d_d/(d_V0+d_V1)$
d_Vrt:(t/d_t)*(d_V1r-d_V0r)+d_V0r$
d_Ea:1/2*d_rho_air*d_SCx*factor(integrate(d_Vrt^3,t,0,d_t))$
Poly:ratsimp((d_Ec+d_Ea+d_Ep+d_Er)/d_t-d_P,d_V1)$

a:ratcoef(Poly,d_V1^3)$
b:ratcoef(Poly,d_V1^2)$
c:ratcoef(Poly,d_V1)$
d:ev(Poly,d_V1=0)$

stringout("calc_model_a.out",ratsimp(a))$
stringout("calc_model_b.out",ratsimp(b))$
stringout("calc_model_c.out",ratsimp(c))$
stringout("calc_model_d.out",ratsimp(d))$
stringout("calc_model_Ea.out",ratsimp(d_Ea))$
stringout("calc_model_Ec.out",ratsimp(d_Ec))$

kill(a,b,c,d)$

p:(3*a*c-b**2)/(3*a^2)$
q:(2*b**3-9*a*b*c+27*a**2*d)/(27*a**3)$
ecart:-b/(3*a)$

stringout("calc_model_p.out",ratsimp(p))$
stringout("calc_model_q.out",ratsimp(q))$

kill(p,q)$

discri:27*q**2+4*p**3$
stringout("calc_model_discri.out",discri)$
Q:V**3+p*V+q$

forget(facts())$
assume(discri>0)$
sol:realpart(subst(solve(Q=0,V)[3],V))$
stringout("calc_model_sol_pos.out",ratsimp(sol)+ecart)$

forget(facts())$
assume(discri<0)$
sol:realpart(subst(solve(Q=0,V)[3],V))$
stringout("calc_model_sol_neg.out",ratsimp(sol)+ecart)$

