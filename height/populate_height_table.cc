#include "populate_height_table.h"

int main(int argc, char* argv[])
{
    if(argc!=7)
    {
        fprintf(stderr,"Usage : %s \"database connection string\" srtm_dir/ lon_min lat_min lon_max lat_max\n",argv[0]);
        return(1);
    }

    std::string s_db = argv[1];
    std::string s_srtm_dir = argv[2];
    int lon_min, lon_max, lat_min, lat_max;
    sscanf(argv[3],"%i",&lon_min);
    sscanf(argv[4],"%i",&lat_min);
    sscanf(argv[5],"%i",&lon_max);
    sscanf(argv[6],"%i",&lat_max);


    fprintf(stderr,"Openning database\n");
    pqxx::connection db(s_db);


    fprintf(stderr,"Deleting points of non original points of grid\n");

    {
        pqxx::work txn(db);

        txn.exec("DELETE FROM rv_height WHERE approx;");
        
        txn.commit();
    }
    /*
    fprintf(stderr,"Unset node heights computed with non orignal points of grid\n");

    {
        pqxx::work txn(db);

        txn.exec("UPDATE nodes SET (height,height_version,height_with_only_originals)=(NULL,0,TRUE) WHERE NOT height_with_only_originals;");
        
        txn.commit();
    }
    */

    for(int lon_iter=lon_min; lon_iter<lon_max; lon_iter++)
    {
        for(int lat_iter=lat_min; lat_iter<lat_max; lat_iter++)
        {
            fprintf(stderr,"Inserting grid points where (lon,lat) in [%i,%i[ x [%i,%i[\n",lon_iter,lon_iter+1,lat_iter,lat_iter+1);

            std::string s_dfilename = s_srtm_dir;
            {
                char dN = (lat_iter>=0) ? 'N' : 'S' ;
                int dNs = (lat_iter>=0) ? 1 : -1 ;
                char dE = (lon_iter>=0) ? 'E' : 'W' ;
                int dEs = (lon_iter>=0) ? 1 : -1 ;

                char fname[1024];
                snprintf(fname,1024, "/%c%02u%c%03u.hgt",dN,dNs*lat_iter,dE,dEs*lon_iter);
                s_dfilename.append(fname);
            }

            
            fprintf(stderr,"    Opening %s\n",s_dfilename.c_str());
            std::ifstream ifs(s_dfilename.c_str());

            if(!ifs.is_open())
            {
                fprintf(stderr,"    Error openning file %s\n",s_dfilename.c_str());
                fprintf(stderr,"    Skipping...\n");
                continue;
            }

            pqxx::work txn(db);

            for(int lat_iter_3=3600; lat_iter_3>=0; lat_iter_3-=3)
            {
                for(int lon_iter_3=0; lon_iter_3<=3600; lon_iter_3+=3)
                {
                    if(ifs.eof())
                    {
                        fprintf(stderr,"Unexpected end of file, lat_iter_3 = %i ; lon_iter_3 = %i\n",lat_iter_3,lon_iter_3);
                        abort();
                    }

                    char c[2];
                    ifs.read(c,2);

                    short s;
                    s=(unsigned char)(c[0]);
                    s=s<<8;
                    s+=(unsigned char)(c[1]);
                    
                    if(s!=-32768 && lon_iter_3!=3600 && lat_iter_3!=3600)
                    {
                        int lon_s=3600*lon_iter+lon_iter_3;
                        int lat_s=3600*lat_iter+lat_iter_3;

                        double lon = lon_s * 1.0/3600;
                        double lat = lat_s * 1.0/3600;

                        char req2[1024];
                        snprintf(req2,1024,"INSERT INTO rv_height (lon_s,lat_s,height,geom,approx) VALUES (%i,%i,%i,'SRID=4326;POINT(%.6f %.6f)'::geometry,FALSE);",lon_s,lat_s,s,lon,lat);
                        
                        txn.exec(req2);
                    }

                }
            }

            txn.commit();
        }
    }


    fprintf(stderr,"\nDone!\n");
    return(0);

}
