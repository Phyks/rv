Introduction
============

Ceci présente comment installer une base osmosis et les tables spécifiques à rv,
ainsi que le backend de routage. Si vous avez déjà une base osmosis pgsnapshot avec une
mise à jour, vous pouvez ajouter les tables spécifiques à rv dessus.

Si vous n'avez pas de base osmosis pgsnapshot, passer à la partie suivante.

La suite de ce manuel parle de la création d'une base à partir de rien. Si vous
avez déjà une base osmosis pgsnapshot fonctionnelle, vous devez juste effectuez les étapes
suivantes.

 * Installer les dépendances de RV, le télécharger, le compiler et l'installer.

 * Vous assurer que la table actions est activée dans votre base, sinon importez
   le schema.

 * Importer le schema SQL de rv, les fonctions, et le timing. (Mais pas
   `scripts/osmosisUpdate.sql`)

 * Si vous avez une fonction `osmosisUpdate()` de définie, ajoutez-y
   `PERFORM rv_fill_to_update();`. Sinon importer `osmosisUpdate.sql`.

 * Ajoutez l'execution du script sql d'update (`scripts/update.sql`) à une
   crontab, moins fréquente que la mise à jour de la base osmosis.

 * Réaliser l'import initial (`scripts/import-initial.sql`).

Veuillez également prendre note des points suivants:

 * Pendant la mise à jour des tables spécifiques à rv, un vérrou en écriture (la
   lecture n'est pas vérrouillée) est posé sur des tables. Ainsi la mise à jour
   de la partie non spécifique à RV de la base sera mise en attente. Ceci n'est
   généralement pas un problème.

 * Pendant l'import initial des tables spécifiques à rv, un verrou en écriture
   (la lecture n'est pas verrouillée) est posé sur des tables. Ainsi la mise à
   jour de la partie non spécifique à RV de la base sera mise en attente. Comme
   l'import initial est une opération longue, ceci peut être un problème pour
   vous. Vous pouvez supprimer les verrous dans le script d'import, mais dans ce
   cas aucune garantie de cohérence de la base n'est apportée, et si on met la
   main dans un tondeuse, on peut s'attendre à avoir mal.

Prérequis sur le serveurs
=========================


Postgres, postgis, installation
-------------------------------

    apt-get install \
        postgis \
        postgresql-VERSION-postgis-scripts \
        osmosis \
        postgresql-contrib \
        postgresql-plperl

Conf de postgres
----------------

Ceci n'est qu'un exemple. `/etc/postgresql/VERSION/main/postgresql.conf`

    shared_buffers = 2GB
    temp_buffers = 256MB
    max_prepared_transactions = 1024
    work_mem = 1GB
    maintenance_work_mem = 1GB
    max_stack_depth = 7MB

Pour l'import initial, on se fout de la coherence en cas de crash. Ou alors on
est patient, très patient, très très patient.

    fsync = off
    synchronous_commit = off
    full_page_writes = off

Penser à revenir sur une configuration plus safe après l'import.

Penser à redémarrer postgresql (service, systemctl…)

RV
--

    apt-get install \
        build-essential \
        autoconf \
        automake \
        libpqxx3-dev \
        git

Puis on a besoin d'un user non privilégié rv, et on fait en sorte de pouvoir s'y
connecter en ssh.

    useradd -m -s /bin/bash rv
    su rv -c 'mkdir /home/rv/.ssh'
    su rv -c 'touch /home/rv/.ssh/authorized_keys'
    cat .ssh/authorized_keys > /home/rv/.ssh/authorized_keys

Compilation et installation des sources
=======================================

Avec un utilisateur non privilégié nommé rv

    git clone https://gitlab.crans.org/leger/rv.git

    cd rv
    sh autogen.sh
    ./configure --prefix=$HOME/local
    make
    make install


Import des données
==================

Création de la database
-----------------------

    # creation database en temps qu'utilisateur postgres
    createuser rv
    createdb -O rv rv
    psql -d rv -c 'CREATE EXTENSION hstore;'
    createlang plperl rv
    psql -d rv \
        -f /usr/share/postgresql/VERSION/contrib/postgis-VERSION/postgis.sql
    psql -d rv \
        -f /usr/share/postgresql/VERSION/contrib/postgis-VERSION/spatial_ref_sys.sql

Schéma de la base (en tant que rv)

    psql -d rv \
        -f /usr/share/doc/osmosis/examples/pgsnapshot_schema_0.6.sql
    psql -d rv \
        -f /usr/share/doc/osmosis/examples/pgsnapshot_schema_0.6_action.sql

Ajouts pour rv

    psql -d rv -f local/share/rv/sql/schemas/base.sql
    psql -d rv -f local/share/rv/sql/schemas/height.sql
    cat local/share/rv/sql/functions/*.sql | psql -d rv
    psql -d rv -f local/share/rv/sql/misc/osmosisUpdate.sql

Si on veut activer le timing (stoqué dans une table rv_update_timing) on

    psql -d rv -f local/share/rv/sql/schemas/timing.sql
    psql -d rv -f local/share/rv/sql/misc/timing-enable.sql

Sinon

    psql -d rv -f local/share/rv/sql/misc/timing-disable.sql

On peut désactiver le timing à tout moment (et éventuellement supprimer la
table). Le timing est négligeable en taille sur la base et en temps, mais ça
fait une table de plus.

Import des données OSM
----------------------

Téléchargement :

    mkdir data/
    mkdir data/import
    wget -P data/  \
        http://download.geofabrik.de/europe/france-latest.osm.pbf

alternative:
http://download.geofabrik.de/north-america/us/pennsylvania-latest.osm.pbf

Noter la date du fichier d'import. Elle sera utile un jour.

    osmosis \
        --read-pbf file=data/france-latest.osm.pbf \
        --write-pgsql-dump directory=data/import/

Import initial (en tant que postgres pour avoir le droit de COPY):

    dir=$(pwd)/data/import
    (
        echo "COPY nodes FROM '$dir/nodes.txt';"
        echo "COPY relation_members FROM '$dir/relation_members.txt';"
        echo "COPY relations FROM '$dir/relations.txt';"
        echo "COPY users FROM '$dir/users.txt';"
        echo "COPY way_nodes FROM '$dir/way_nodes.txt';"
        echo "COPY ways FROM '$dir/ways.txt';"
    ) \
        | xargs -d '\n' -i -P 0 psql -d rv -c {}

Une petite analyse de ce qu'on a mis :

    psql -d rv -c "VACUUM ANALYZE;"

Menage partiel

    rm -r data/import
    rm data/france-latest.osm.pbf

Import des données SRTM
-----------------------

Récuperer la liste des tiles srtm necessaires

    psql -d rv \
        -t -F ' ' -A \
        -c 'SELECT DISTINCT floor(ST_X(geom)),floor(ST_Y(geom)) from nodes;' \
        > data/list_needed_srtm


Telecharger les données srtm
(il y a plein de 404 c'est normal)

    mkdir data/srtmzip
    local/share/rv/bin/dlsrtm data/list_needed_srtm data/srtmzip/

Dézippage

    mkdir data/srtm
    local/share/rv/bin/unzipsrtm data/srtmzip/ data/srtm/

Remplissage paralelle de la database

    local/share/rv/bin/populate_height_table_parallel \
        data/list_needed_srtm 12 \
        local/share/rv/bin/populate_height_table dbname=rv data/srtm/


Une petite analyse de ce qu'on a mis :

    psql -d rv -c "VACUUM ANALYZE rv_height;"

Ménage

    rm -r data


Remplissage initial des tables perso
------------------------------------

    psql -d rv -f local/share/rv/sql/scripts/initial-import.sql
    psql -d rv -c "VACCUM ANALYZE;"



Configuration de la mise à jour de la base de donnée (partie osmosis)
=====================================================================

Création du répertoire et config
--------------------------------

    mkdir update_routing_db
    osmosis --rrii workingDirectory=update_db/

Etiter le fichier `update_routing_db/configuration.txt` pour mettre les deux lignes :

    baseUrl=http://download.geofabrik.de/europe/france-updates
    maxInterval = 1

`maxInterval` à `1` permet de forcer l'application des diffs un à un (à moins
qu'il y ai plusieurs diff par secondre). En théorie on peut appliquer plusieurs
diff en même temps, mais osmosis peut planter lorsqu'un nœud est supprimé après
avoir été crée ou modifié dans un diff antérieur.

Telechager un state.txt plus vieux que l'import. On peut prendre un peu de
marge, ça ne mange pas de pain.

Script d'update
---------------

J'utilise ce script d'update (à part le log ce n'est qu'une commande osmosis
dans une boucle)

    #!/bin/bash

    dir=/home/rv/update_db/

    (
        printf "Begin: $(date +'%Y-%m-%d %H:%M:%S')\n\n" >&2

        printf "Old state\n\n" >&2
        cat $dir/state.txt >&2
        printf "\n\n" >&2

        k=0
        seq_number=$(sed -ne '/sequenceNumber/s/^.*=//p' $dir/state.txt)
        finish=0

        while test $finish -eq 0
        do
            ((k++))
            old_seq_number=$seq_number

            printf "Osmosis, execution number $k\n\n" >&2

            osmosis \
                --read-replication-interval workingDirectory=$dir \
                --simplify-change \
                --write-pgsql-change database=rv user=rv password=lR0Jqttl1GsQ

            printf "\n\n" >&2

            printf "New state\n\n" >&2
            cat $dir/state.txt >&2
            printf "\n\n" >&2

            seq_number=$(sed -ne '/sequenceNumber/s/^.*=//p' $dir/state.txt)

            if test $seq_number -eq $old_seq_number
            then
                finish=1
            fi
        done

        printf "End: $(date +'%Y-%m-%d %H:%M:%S')\n" >&2

    ) 2> $dir/log/$(date +%Y-%m-%d_%H-%M-%S).log


Puis le placer dans une crontab. Fréquence du cron… quotidien semble un bon
choix.


Mise à jour des tables pour le routage
======================================

Il est inutile (et débile) de faire une mise à jour plus fréquente que celle de
la partie osmosis (partie précedente).

Il suffit d'executer le script SQL. `local/share/rv/sql/scripts/update.sql`.

On peut choisir de mettre la commande suivante dans une crontab:

    psql -d rv '/home/rv/local/share/rv/sql/scripts/update.sql'

Fréquence du cron… hebdomadaire semble un bon choix.
