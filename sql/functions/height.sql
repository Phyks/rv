
DROP FUNCTION IF EXISTS get_height(FLOAT,FLOAT);
CREATE OR REPLACE FUNCTION get_height(FLOAT,FLOAT) RETURNS FLOAT AS $$

    sub floor
    {
        my ($value) = @_;
        if($value>0)
        {
            return(int($value));
        }

        if(int($value)==$value)
        {
            return($value);
        }

        return(int($value)-1);
    }

    sub get_grid
    {
        my ($lon_s,$lat_s) = @_;

        my $rv = spi_exec_query("SELECT height FROM rv_height WHERE (lon_s,lat_s)=($lon_s,$lat_s);");
        if($rv->{processed}>0)
        {
            return $rv->{rows}[0]->{height};
        }
        else
        {
            my $lon=$lon_s*1.0/3600;
            my $lat=$lat_s*1.0/3600;
            my $diam=0.003;

            my $found = 0;
            my $computed;
            while($found==0)
            {
                my $query=sprintf("SELECT height FROM rv_height WHERE geom && ST_Expand('SRID=4326;POINT(%.8f %.8f)'::geometry,%.8f) AND NOT approx ORDER BY ST_Distance(geom,'SRID=4326;POINT(%.8f %.8f)'::geography) LIMIT 4;",$lon,$lat,$diam,$lon,$lat);
                
                my $rv=spi_exec_query($query);
                if($rv->{processed}<4)
                {
                    $diam=$diam*2;
                }
                else
                {
                    $computed = ($rv->{rows}[0]->{height}+$rv->{rows}[1]->{height}+$rv->{rows}[2]->{height}+$rv->{rows}[3]->{height})*.25;
                    $found=1;
                }

                if($diam>2)
                {
                    return undef;
                }

            }
            my $query=sprintf("INSERT INTO rv_height (lon_s,lat_s,height,geom,approx) VALUES (%d,%d,%f,'SRID=4326;POINT(%.8f %.8f)'::geometry,TRUE);",$lon_s,$lat_s,$computed,$lon,$lat);
            spi_exec_query($query);
            return $computed;

        }
    }

    my ($lon,$lat) = @_;

    my $lon_s_0 = floor($lon*1200)*3;
    my $lat_s_0 = floor($lat*1200)*3;
    my $lon_s_1 = $lon_s_0 + 3;
    my $lat_s_1 = $lat_s_0 + 3;

    my $h00 = get_grid($lon_s_0,$lat_s_0);
    my $h01 = get_grid($lon_s_0,$lat_s_1);
    my $h10 = get_grid($lon_s_1,$lat_s_0);
    my $h11 = get_grid($lon_s_1,$lat_s_1);

    if(!defined($h00) || !defined($h01) || !defined($h10) || !defined($h11))
    {
        return undef;
    }

    my $x = (3600*$lon-$lon_s_0)/3;
    my $y = (3600*$lat-$lat_s_0)/3;

    my $hx0 = $h00*(1-$x)+$h10*$x;
    my $hx1 = $h01*(1-$x)+$h11*$x;

    my $hxy = $hx0*(1-$y)+$hx1*$y;


    return $hxy;

$$ LANGUAGE plperl;

