
DROP FUNCTION IF EXISTS compute_edges_for_one_way(BIGINT);
CREATE FUNCTION compute_edges_for_one_way(BIGINT) RETURNS INTEGER AS $$
    my ($wayid) = @_;
    my $r=spi_exec_query("SELECT tags->'oneway' AS oneway,tags->'cycleway' AS cycleway,tags->'junction' AS junction FROM ways WHERE id=$wayid;")->{rows}[0];

    my $sens=0;
    my $oneway = $r->{oneway};
    my $cycleway = $r->{cycleway};
    my $junction = $r->{junction};

    if(defined($oneway))
    {
        if( $oneway=~m/(yes|1|true)/ )
        {
            $sens=1;
        }
        if( $oneway=~m/-1/ )
        {
            $sens=-1;
        }
    }

    if(defined($junction))
    {
        if( $junction=~m/roundabout/ )
        {
            $sens=1;
        }
    }

    if(defined($cycleway))
    {
        if($cycleway=~m/opposite/)
        {
            $sens=0;
        }
    }

    my $query2 = "SELECT node_id FROM way_nodes WHERE way_id=$wayid ORDER BY sequence_id ASC;";
    my $res2 = spi_exec_query($query2);

    my $nodeid=-1;
    foreach my $rn2 (0 .. $res2->{processed} - 1)
    {
        my $old=$nodeid;
        $nodeid = $res2->{rows}[$rn2]{node_id};
        if($old!=$nodeid)
        {
            if($old>=0)
            {
                if($sens>=0)
                {
                    spi_exec_query("INSERT INTO rv_edges (from_id,to_id,way_id) VALUES ($old,$nodeid,$wayid);");
                }
                if($sens<=0)
                {
                    spi_exec_query("INSERT INTO rv_edges (from_id,to_id,way_id) VALUES ($nodeid,$old,$wayid);");
                }

            }
        }
    }

    return 0;
$$ LANGUAGE plperl;
