
BEGIN;
    
    -- locks only for wrinting this is not blocking reading
    LOCK TABLE nodes IN EXCLUSIVE MODE;
    LOCK TABLE ways IN EXCLUSIVE MODE;
    LOCK TABLE way_nodes IN EXCLUSIVE MODE;
    LOCK TABLE actions IN EXCLUSIVE MODE;
    LOCK TABLE rv_nodes IN EXCLUSIVE MODE;
    LOCK TABLE rv_ways IN EXCLUSIVE MODE;
    LOCK TABLE rv_edges IN EXCLUSIVE MODE;
    
    -- this is blocking reading and wrinting
    LOCK TABLE rv_to_update IN ACCESS EXCLUSIVE MODE;


    -- we are alone to write, mouhahahahaha
    SELECT updateRV();

    TRUNCATE rv_to_update;

    -- we write and release locks
COMMIT;


