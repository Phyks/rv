
BEGIN;
    -- rv_ways
    DROP TABLE IF EXISTS rv_ways;
    CREATE TABLE rv_ways (id BIGINT);
    ALTER TABLE rv_ways ADD CONSTRAINT pk_rv_ways PRIMARY KEY (id);

    -- rv_nodes
    DROP TABLE IF EXISTS rv_nodes;
    CREATE TABLE rv_nodes (id BIGINT, height FLOAT, cc BIGINT);
    SELECT AddGeometryColumn('rv_nodes', 'geom', 4326, 'POINT', 2);

    ALTER TABLE rv_nodes ADD CONSTRAINT pk_rv_nodes PRIMARY KEY (id);
    CREATE INDEX idx_rv_nodes_geom ON rv_nodes USING gist (geom);
    CREATE INDEX idx_rv_nodes_cc ON rv_nodes USING btree (cc);

    CLUSTER rv_nodes USING idx_rv_nodes_geom;

    -- rv_edges
    DROP TABLE IF EXISTS rv_edges;
    CREATE TABLE rv_edges (from_id BIGINT, to_id BIGINT, way_id BIGINT);
    ALTER TABLE rv_edges ADD CONSTRAINT pk_rv_edges PRIMARY KEY (from_id,to_id,way_id);

    CREATE INDEX idx_rv_edges_from_id ON rv_edges USING btree (from_id);
    CREATE INDEX idx_rv_edges_way_id ON rv_edges USING btree (way_id);

    -- rv_to_update
    DROP TABLE IF EXISTS rv_to_update;
    CREATE TABLE rv_to_update (data_type character(1) NOT NULL, action character(1) NOT NULL, id bigint NOT NULL);
    ALTER TABLE rv_to_update ADD CONSTRAINT pk_rv_to_update PRIMARY KEY (data_type, id);
    


COMMIT;


